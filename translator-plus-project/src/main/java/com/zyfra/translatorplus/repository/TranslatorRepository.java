package com.zyfra.translatorplus.repository;

import com.zyfra.translatorplus.yaml.repository.YamlRepositoryProperties;
import com.zyfra.translatorplus.yaml.resource.YamlResources;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class TranslatorRepository {

    public YamlRepositoryProperties repositoryProperties;
    public Map<String, YamlResources> resourceMap;

    public TranslatorRepository(){
        repositoryProperties = new YamlRepositoryProperties();
        resourceMap = new HashMap<>();
    }
}
