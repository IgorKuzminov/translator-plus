package com.zyfra.translatorplus.service;

import com.zyfra.translatorplus.generator.ResourceKeyGenerator;
import com.zyfra.translatorplus.repository.TranslatorRepository;
import com.zyfra.translatorplus.yaml.resource.YamlResource;
import com.zyfra.translatorplus.yaml.resource.YamlResources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TranslatorRepositoryService {

    private TranslatorRepository translatorRepository;

    @Autowired
    public TranslatorRepositoryService(TranslatorRepository translatorRepository) {
        this.translatorRepository = translatorRepository;
    }

    /**
     * Returns the collection of resources found by repository name,
     * project name, language name.
     *
     * @param repoName Resource repository name.
     * @param projectName Resource project name.
     * @param langName Resource language name.
     *
     * @return Collection of resources.
     *
     * @see YamlResource
     * @see List
     */
    @NotNull
    public List<YamlResource> allResources(String repoName, String projectName, String langName) {
        String key = new ResourceKeyGenerator().generateKey(repoName, projectName, langName);
        YamlResources yamlResources = this.translatorRepository.resourceMap.get(key);

        return yamlResources != null
                ? yamlResources.resources
                : Collections.emptyList();
    }

    /**
     * Returns the collection any resources found by repository name,
     * project name, language name and resource key.
     *
     * @param repoName Resource repository name.
     * @param projectName Resource project name.
     * @param langName Resource language name.
     * @param resourceKey Resource key.
     *
     * @return Collection of resources.
     *
     * @see YamlResource
     */
    @NotNull
    public List<YamlResource> anyResources(String repoName, String projectName,
                                           String langName, String resourceKey) {
        String key = new ResourceKeyGenerator().generateKey(repoName, projectName, langName);
        YamlResources yamlResources = this.translatorRepository.resourceMap.get(key);

        if (yamlResources == null)
            return Collections.emptyList();

        return yamlResources.resources.stream()
                .filter(r -> r.getKey().equals(resourceKey))
                .collect(Collectors.toList());
    }

    /**
     * Returns the first resource found by repository name,
     * project name, language name and resource key.
     *
     * @param repoName Resource repository name.
     * @param projectName Resource project name.
     * @param langName Resource language name.
     * @param resourceKey Resource key.
     *
     * @return Collection of resources.
     *
     * @see YamlResource
     */
    @Nullable
    public YamlResource firstResource(String repoName, String projectName,
                                      String langName, String resourceKey) {
        String key = new ResourceKeyGenerator().generateKey(repoName, projectName, langName);
        YamlResources yamlResources = this.translatorRepository.resourceMap.get(key);

        if (yamlResources == null)
            return null;

        Optional<YamlResource> optional = yamlResources.resources.stream()
                .filter(r -> r.getKey().equals(resourceKey))
                .findFirst();

        return optional.orElse(null);
    }
}
