package com.zyfra.translatorplus.yaml.repository;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Model of repository-properties.yml.
 */
public class YamlRepositoryProperties {

    @JsonProperty("repositories")
    public List<YamlRepository> repositories = new ArrayList<>();

    @JsonProperty("projects")
    public List<YamlProject> projects = new ArrayList<>();

    @JsonProperty("langs")
    public List<YamlLang> langs = new ArrayList<>();
}
