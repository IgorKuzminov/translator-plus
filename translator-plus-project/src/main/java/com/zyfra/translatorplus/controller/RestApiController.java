package com.zyfra.translatorplus.controller;

import com.zyfra.translatorplus.service.TranslatorRepositoryService;
import com.zyfra.translatorplus.yaml.resource.YamlResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/repo/{repo-name}/project/{project-name}/lang/{lang-name}/resource", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
public class RestApiController {

    private TranslatorRepositoryService service;

    @Autowired
    public RestApiController(TranslatorRepositoryService service) {
        this.service = service;
    }

    @RequestMapping(value = "/all")
    public List<YamlResource> allResources(
            @PathVariable("repo-name") String repoName,
            @PathVariable("project-name") String projectName,
            @PathVariable("lang-name") String langName) {

        return this.service.allResources(repoName, projectName, langName);
    }

    @RequestMapping(value = "/{resource-key}/any")
    public List<YamlResource> allResources(
            @PathVariable("repo-name") String repoName,
            @PathVariable("project-name") String projectName,
            @PathVariable("lang-name") String langName,
            @PathVariable("resource-key") String resourceKey) {

        return this.service.anyResources(repoName, projectName, langName, resourceKey);
    }

    @RequestMapping(value = "/{resource-key}/first")
    public YamlResource firstResource(
            @PathVariable("repo-name") String repoName,
            @PathVariable("project-name") String projectName,
            @PathVariable("lang-name") String langName,
            @PathVariable("resource-key") String resourceKey) {

        return this.service.firstResource(repoName, projectName, langName, resourceKey);
    }
}
