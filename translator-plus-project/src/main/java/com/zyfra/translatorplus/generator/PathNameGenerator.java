package com.zyfra.translatorplus.generator;

import javax.validation.constraints.NotNull;
import java.io.File;

public class PathNameGenerator {

    public String generateRepositoryPropertiesPathName(@NotNull String repositoriesDirPathName, @NotNull String repositoryPropertiesFileName){
        return repositoriesDirPathName + File.separator + repositoryPropertiesFileName;
    }

    public String generateResourceFilePathName(@NotNull String repositoriesPathName, @NotNull String repoDirName,
                                               @NotNull String projectDirName, @NotNull String langDirName,
                                               @NotNull String resFileName){
        return repositoriesPathName + File.separator + repoDirName + File.separator + projectDirName
                + File.separator + langDirName + File.separator + resFileName;
    }
}
