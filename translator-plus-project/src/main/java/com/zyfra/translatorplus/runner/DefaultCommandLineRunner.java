package com.zyfra.translatorplus.runner;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class DefaultCommandLineRunner implements CommandLineRunner {

    private final Log log = LogFactory.getLog(DefaultCommandLineRunner.class);
    private final Environment environment;

    @Value("${server.port}")
    private String serverPort;

    @Value("${spring.application.name}")
    private String appName;

    @Autowired
    public DefaultCommandLineRunner(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void run(String... args) {
        String[] activeProfiles = environment.getActiveProfiles();
        for (String profile : activeProfiles){
            log.info(String.format("Use profile: %s", profile));
        }

        log.info(String.format("%s running on port %s!", appName, serverPort));
    }
}
