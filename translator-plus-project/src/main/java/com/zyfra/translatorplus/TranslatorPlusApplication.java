package com.zyfra.translatorplus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.util.Assert;

@SpringBootApplication
@EnableDiscoveryClient
@EnableAsync
public class TranslatorPlusApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(TranslatorPlusApplication.class);
		setProfiles(app, "thymeleaf", "consul");
		app.run(args);
	}

	private static void setProfiles(SpringApplication app, String... profiles) {
		Assert.notNull(app, "app can not be null");
		app.setAdditionalProfiles(profiles);
	}}
