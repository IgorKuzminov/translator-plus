package com.zyfra.translatorplus.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TranslatorRepositoryConfiguration {

    public final String REPOSITORY_PROPERTIES_FILE = "repository-properties.yml";
    public final String RESOURCES_FILE = "resources.yml";

    @Value("${repo}")
    public String repositoriesPath;
}
