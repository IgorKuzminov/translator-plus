# HTTP API
The main interface to Translator Plus is a RESTful HTTP API. The API can perform basic CRUD operations.

## Collection of all resources
This endpoint returns the collection of resources found by repository name, project name, and language name.

### Methods
|Method    |Path                                                               |Produces
-----------|-------------------------------------------------------------------|-------------------------
GET        |/repo/{repo-name}/project/{project-name}/lang/{lang-name}/resource |application/json

#### Parameters
|Parameter       |Description                |Type
-----------------|---------------------------|-----------------------
repo-name        |resource repository name   |string
project-nam      |resource project name      |string
lang-name        |resource language name     |string

#### Sample

##### Curl
```
curl -X GET -i http://localhost:10000/repo/common/project/mdc/lang/ru/resource/all
```

##### Request URL
```
http://localhost:10000/repo/common/project/mdc/lang/ru/resource/all
```

##### Response body
```
[{
  "key": "app-name",
  "value": "АИС Диспетчер"
}, {
  "key": "monitoring-module-name",
  "value": "Мониторинг"
}, {
  "key": "report-module-name",
  "value": "Отчеты"
}]
```


