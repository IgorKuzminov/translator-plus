package com.zyfra.translatorplus;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URL;

//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//public class RestApiControllerTest {
//
//    @LocalServerPort
//    private int port;
//
//    private URL url;
//
//    @Autowired
//    private TestRestTemplate template;
//
//    @Before
//    public void setUp() throws Exception {
//        this.url = new URL("http://localhost:" + port + "/getElement");
//    }
//
//    @Test
//    public void getElement() {
//        String url = this.url.toString();
//
//        ResponseEntity<String> response = template
//                .getForEntity(url, String.class);
//
//        String body = response.getBody();
//
//        assertThat(body, notNullValue());
//    }
//}
