package com.zyfra.translatorplus.yaml.resource;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class YamlResource {

    @JsonProperty("key")
    private String key;

    @JsonProperty("value")
    private String value;
}
