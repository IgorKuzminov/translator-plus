package com.zyfra.translatorplus.yaml.resource;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Model of resources.yml.
 */
public class YamlResources {

    @JsonProperty("resources")
    public List<YamlResource> resources = new ArrayList<>();
}
