package com.zyfra.translatorplus.generator;

import javax.validation.constraints.NotNull;

public class ResourceKeyGenerator {

    public String generateKey(@NotNull String repoName, @NotNull String projectName, @NotNull String langName){
        return String.format("%s:%s:%s", repoName, projectName, langName);
    }
}
