package com.zyfra.translatorplus.loader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.zyfra.translatorplus.configuration.TranslatorRepositoryConfiguration;
import com.zyfra.translatorplus.generator.ResourceKeyGenerator;
import com.zyfra.translatorplus.generator.PathNameGenerator;
import com.zyfra.translatorplus.repository.TranslatorRepository;
import com.zyfra.translatorplus.yaml.repository.YamlLang;
import com.zyfra.translatorplus.yaml.repository.YamlProject;
import com.zyfra.translatorplus.yaml.repository.YamlRepository;
import com.zyfra.translatorplus.yaml.repository.YamlRepositoryProperties;
import com.zyfra.translatorplus.yaml.resource.YamlResources;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.HashMap;

@Component
public class TranslatorRepositoryLoader {

    private final Log log = LogFactory.getLog(TranslatorRepositoryLoader.class);

    private TranslatorRepositoryConfiguration repositoryConfiguration;
    private TranslatorRepository translatorRepository;

    @Autowired
    public TranslatorRepositoryLoader(TranslatorRepositoryConfiguration repositoryConfiguration, TranslatorRepository translatorRepository){

        this.repositoryConfiguration = repositoryConfiguration;
        this.translatorRepository = translatorRepository;
    }

    @PostConstruct
    public void load(){
        log.info(String.format("Loading repositories from path: %s ...", repositoryConfiguration.repositoriesPath));

        PathNameGenerator pathGenerator = new PathNameGenerator();
        final String repositoryPropertiesPathName = pathGenerator.generateRepositoryPropertiesPathName(repositoryConfiguration.repositoriesPath, repositoryConfiguration.REPOSITORY_PROPERTIES_FILE);
        File repositoryPropertiesFile = new File(repositoryPropertiesPathName);
        if (!repositoryPropertiesFile.exists()){
            log.info(String.format("File %s not found", repositoryPropertiesPathName));
            return;
        }

        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        ResourceKeyGenerator keyGenerator = new ResourceKeyGenerator();
        try {
            // use mapper.writeValue for write values
            translatorRepository.repositoryProperties = mapper.readValue(repositoryPropertiesFile, YamlRepositoryProperties.class);

            translatorRepository.resourceMap = new HashMap<>();
            for (YamlRepository yamlRepository : translatorRepository.repositoryProperties.repositories){
                for (YamlProject yamlProject : translatorRepository.repositoryProperties.projects) {
                    for (YamlLang yamlLang : translatorRepository.repositoryProperties.langs){
                        String resourceFilePathName = pathGenerator.generateResourceFilePathName(repositoryConfiguration.repositoriesPath,
                                yamlRepository.getDirectory(), yamlProject.getDirectory(),
                                yamlLang.getDirectory(), repositoryConfiguration.RESOURCES_FILE);

                        File resourceFile = new File(resourceFilePathName);
                        try {
                            YamlResources yamlResource = mapper.readValue(resourceFile, YamlResources.class);
                            String key = keyGenerator.generateKey(yamlRepository.getName(), yamlProject.getName(), yamlLang.getName());

                            translatorRepository.resourceMap.put(key, yamlResource);
                        }
                        catch (Exception e) {
                            log.error(String.format("Error read file %s", resourceFilePathName), e);
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            log.error(String.format("Error read file %s", repositoryPropertiesPathName), e);
        }

        log.info(String.format("Resource files count: %s", translatorRepository.resourceMap.size()));
    }
}
