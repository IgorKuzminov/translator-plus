# Translator Plus
Translator Plus allows you to create and maintain a repositories of translations for devepment projects for different programming languages.

## Technology
 1. Java SE Platform 11
 2. Apache Maven
 3. Spring Boot 2.x: Spring Web MVC|Spring Data REST|Spring Security|Spring Cloud Consul
 4. Swagger
 5. Log4J
 6. Thymeleaf & AngularJS
 7. React & Material-UI
 
## Dependencies
1. [Git](https://git-scm.com/downloads), of course, installed on your local machine.
2. [Maven](https://maven.apache.org/) to compile and run the project.

Assuming you have Git and Maven on your local machine you will run the following commands. On the terminal of your
choice change directories to where you want the cloned project files to download and run:

```
git clone https://gitlab.com/IgorKuzminov/translator-plus.git
```
